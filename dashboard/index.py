import dash_core_components as dcc
import dash_html_components as html

layout_index = html.Div([
    dcc.Link('Navigate to "/page-1"', href='/dashboard/page-1'),
    html.Br(),
    dcc.Link('Navigate to "/page-2"', href='/dashboard/page-2'),
])