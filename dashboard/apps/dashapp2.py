import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input
from dash.dependencies import Output

from app import create_app

server = create_app()
dashapp = dash.Dash(__name__, server=server, url_base_pathname='/dashboard/')
dashapp.config['suppress_callback_exceptions'] = True


layout_page_2 = html.Div([
    html.H2('Page 2'),
    dcc.Dropdown(
        id='page-2-dropdown',
        options=[{'label': i, 'value': i} for i in ['LA', 'NYC', 'MTL']],
        value='LA'
    ),
    html.Div(id='page-2-display-value'),
    html.Br(),
    dcc.Link('Navigate to "/"', href='/dashboard/'),
    html.Br(),
    dcc.Link('Navigate to "/page-1"', href='/dashboard/page-1'),
])


# Page 2 callbacks
@dashapp.callback(Output('page-2-display-value', 'children'),
              [Input('page-2-dropdown', 'value')])
def display_value(value):
    print('display_value')
    return 'You have selected "{}"'.format(value)


