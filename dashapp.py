import flask
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input
from dash.dependencies import Output
from flask_login import login_required

from app import create_app

from dashboard import index
from dashboard.apps import dashapp1, dashapp2


def protect_dashviews(dashapp):
    for view_func in dashapp.server.view_functions:
        if view_func.startswith(dashapp.url_base_pathname):
            dashapp.server.view_functions[view_func] = login_required(dashapp.server.view_functions[view_func])


server = create_app()

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


dashapp = dash.Dash(__name__, server=server, url_base_pathname='/dashboard/', external_stylesheets=external_stylesheets)
protect_dashviews(dashapp)


url_bar_and_content_div = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


# Ensures passing full validation test ( runs before request_context is available )
def serve_layout():
    if flask.has_request_context():
        return url_bar_and_content_div
    return html.Div([
        url_bar_and_content_div,
        index.layout_index,
        dashapp1.layout_page_1,
        dashapp2.layout_page_2,
    ])


dashapp.layout = serve_layout


# Index callbacks
@dashapp.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == "/dashboard/page-1":
        return dashapp1.layout_page_1
    elif pathname == "/dashboard/page-2":
        return dashapp2.layout_page_2
    else:
        return index.layout_index


# =============================
# Dash app
# =============================
#
# dashapp.layout = html.Div([
#     html.H1('Stock Tickers'),
#     dcc.Dropdown(
#         id='my-dropdown',
#         options=[
#             {'label': 'Coke', 'value': 'COKE'},
#             {'label': 'Tesla', 'value': 'TSLA'},
#             {'label': 'Apple', 'value': 'AAPL'}
#         ],
#         value='COKE'
#     ),
#     dcc.Graph(id='my-graph')
# ], style={'width': '500'})
#
#
# @dashapp.callback(Output('my-graph', 'figure'), [Input('my-dropdown', 'value')])
# def update_graph(selected_dropdown_value):
#     df = pdr.get_data_yahoo(selected_dropdown_value, start=dt(2017, 1, 1), end=dt.now())
#     return {
#         'data': [{
#             'x': df.index,
#             'y': df.Close
#         }],
#         'layout': {'margin': {'l': 40, 'r': 0, 't': 20, 'b': 30}}
#     }

# =============================
# Another dash app
# =============================
# ...
